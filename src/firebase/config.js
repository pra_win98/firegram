import firebase from "firebase/app";
import "firebase/storage";
import "firebase/firestore";

// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyDScWCmdMY6FkLOgsnIBXoBsGxpf_AKksA",
  authDomain: "firegram-cd86e.firebaseapp.com",
  projectId: "firegram-cd86e",
  storageBucket: "firegram-cd86e.appspot.com",
  messagingSenderId: "748541374626",
  appId: "1:748541374626:web:829bb15f46ed3226fc67fb",
});

const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { projectStorage, projectFirestore, timestamp };
